﻿namespace API.Core.Dtos;

public class GetPhoto
{
    public byte[]? Photo { get; set; } = null!;
}