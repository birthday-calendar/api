﻿using API.Core.Entities;

namespace API.Core.Dtos;

public class RemovePersonDto
{
    public int Id { get; set; }

    public Person ToEntity()
    {
        return new(Id, string.Empty, DateTime.MinValue, Array.Empty<byte>());
    }
}