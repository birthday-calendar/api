﻿using API.Core.Entities;
using Microsoft.AspNetCore.Http;

namespace API.Core.Dtos;

public class UpdatePersonDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public DateTime Birthday { get; set; }
    public IFormFile Photo { get; set; } = null!;

    public Person ToEntity()
    {
        long length = Photo.Length;
        byte[] bytes = new byte[length];
        //IFormFile to byte[]
        using var fileStream = Photo.OpenReadStream();
        fileStream.Read(bytes, 0, (int)Photo.Length);
        
        return new(Id, Name, Birthday, bytes);
    }
}