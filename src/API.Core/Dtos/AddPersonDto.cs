﻿using API.Core.Entities;
using Microsoft.AspNetCore.Http;

namespace API.Core.Dtos;

public class AddPersonDto
{
    public string Name { get; set; } = string.Empty;
    public DateTime Birthday { get; set; }
    public IFormFile Photo { get; set; } = null!;

    public Person ToEntity()
    {
        long length = Photo.Length;
        byte[] bytes = new byte[length];
        using var fileStream = Photo.OpenReadStream();
        fileStream.Read(bytes, 0, (int)Photo.Length);
        
        return new(null, Name, Birthday, bytes);
    }
}