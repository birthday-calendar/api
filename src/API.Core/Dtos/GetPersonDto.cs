﻿namespace API.Core.Dtos;

public class GetPersonDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public DateTime Birthday { get; set; }
    public bool IsNear { get; set; }
    public bool IsToday { get; set; }
    public bool IsPast { get; set; }
}