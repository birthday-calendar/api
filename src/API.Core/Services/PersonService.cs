﻿using API.Core.Dtos;
using API.Core.Interfaces;

namespace API.Core.Services;

public class PersonService : IPersonService
{
    private readonly IPersonRepository _repository;
    
    public PersonService(IPersonRepository repository)
    {
        _repository = repository;
    }
    
    public IEnumerable<GetPersonDto> GetAll()
    {
        var persons = _repository.GetAll();
        return persons.Select(p => p.ToGetDto());
    }

    public GetPersonDto? Get(int id)
    {
        var person = _repository.Get(id);
        return person?.ToGetDto();
    }

    public GetPhoto GetPhoto(int personId)
    {
        return new() { Photo = _repository.GetPhoto(personId) };
    }

    public IEnumerable<GetPersonDto> GetNearest()
    {
        var persons = _repository.GetNearest();
        return persons.Select(p => p.ToGetDto());
    }

    public bool Add(AddPersonDto dto)
    {
        return _repository.Add(dto.ToEntity());
    }

    public bool Remove(RemovePersonDto dto)
    {
        return _repository.Remove(dto.ToEntity());
    }

    public bool Update(UpdatePersonDto dto)
    {
        return _repository.Update(dto.ToEntity());
    }
}