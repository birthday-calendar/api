﻿using API.Core.Entities;

namespace API.Core.Interfaces;

public interface IPersonRepository
{
    IEnumerable<Person> GetAll();
    IEnumerable<Person> GetNearest();
    Person? Get(int id);
    byte[]? GetPhoto(int personId);
    bool Add(Person addPerson);
    bool Remove(Person removePerson);
    bool Update(Person updatePerson);
}