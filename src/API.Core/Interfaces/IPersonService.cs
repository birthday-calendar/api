﻿using API.Core.Dtos;

namespace API.Core.Interfaces;

public interface IPersonService
{
    IEnumerable<GetPersonDto> GetAll();
    IEnumerable<GetPersonDto> GetNearest();
    GetPersonDto? Get(int id);
    GetPhoto GetPhoto(int personId);
    bool Add(AddPersonDto dto);
    bool Remove(RemovePersonDto dto);
    bool Update(UpdatePersonDto dto);
}