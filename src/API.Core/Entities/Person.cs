﻿using API.Core.Dtos;

namespace API.Core.Entities;

public class Person
{
    private int? _id;
    private string _name;
    private DateTime _birthday;
    private byte[] _photo;

    public Person(int? id, string name, DateTime birthday, byte[] photo)
    {
        Id = id;
        Name = name;
        Birthday = birthday;
        Photo = photo;
    }

    public int? Id
    {
        get => _id;
        set => _id = value < 0 ? throw new ArgumentOutOfRangeException(nameof(value)) : value;
    }

    public string Name
    {
        get => _name;
        set => _name = value ?? throw new ArgumentNullException(nameof(value));
    }

    public DateTime Birthday
    {
        get => _birthday;
        set => _birthday = value;
    }

    public byte[] Photo
    {
        get => _photo;
        set => _photo = value ?? throw new ArgumentNullException(nameof(value));
    }

    public GetPersonDto ToGetDto()
    {
        var today = DateTime.Today;
        var days = _birthday.Subtract(today).Days;
        return new()
        {
            Id = (int)_id!,
            Name = _name,
            Birthday = _birthday,
            IsNear = days is <= 7 and > 0,
            IsPast = days is < 0 and >= -3,
            IsToday = days is 0
        };
    }

    protected bool Equals(Person other)
    {
        return _id == other._id && _name == other._name && _birthday.Equals(other._birthday) && _photo.Equals(other._photo);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Person)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(_id, _name, _birthday, _photo);
    }

    public static bool operator ==(Person? left, Person? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(Person? left, Person? right)
    {
        return !Equals(left, right);
    }
}