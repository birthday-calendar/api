﻿using API.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace API.Infrastructure.Database.DbContexts;

public sealed class PersonDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; } = null!;

    public PersonDbContext(DbContextOptions<PersonDbContext> options) : base(options)
    {
    }
}