﻿using System.Runtime.InteropServices.JavaScript;
using API.Core.Entities;
using API.Core.Interfaces;
using API.Infrastructure.Database.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace API.Infrastructure.Database.Repositories;

public class DatabasePersonRepository : IPersonRepository
{
    private readonly PersonDbContext _db;

    public DatabasePersonRepository(PersonDbContext dbContext)
    {
        _db = dbContext;
    }

    /// <summary>
    /// Fetch all Person from DB
    /// </summary>
    /// <returns>Return Persons without Photo (empty byte array)</returns>
    public IEnumerable<Person> GetAll()
    {
        return _db.Persons
            .OrderBy(p => p.Birthday)
            .Select(p => new Person(p.Id, p.Name, p.Birthday, Array.Empty<byte>()))
            .AsNoTracking()
            .ToList();
    }
    
    /// <summary>
    /// Fetch all Person which birthday less than 7 days or today
    /// </summary>
    /// <returns>Return Persons without Photo (byte array is empty)</returns>
    public IEnumerable<Person> GetNearest()
    {
        return _db.Persons
            .OrderBy(p => p.Birthday)
            .Where(p =>
                (p.Birthday.DayOfYear - DateTime.Today.DayOfYear) <= 7 &&
                (p.Birthday.DayOfYear - DateTime.Today.DayOfYear) >= 0) //calculating the nearest birthday
            .Select(p => new Person(p.Id, p.Name, p.Birthday, Array.Empty<byte>()))
            .AsNoTracking()
            .ToList();
    }
    
    /// <summary>
    /// Find Person in DB by primary key
    /// </summary>
    /// <param name="id">Primary key</param>
    /// <returns>Return Person without Photo (byte array is empty)</returns>
    public Person? Get(int id)
    {
        var person = _db.Persons.Find(id);
        if (person is null) return null;
        _db.Entry(person).State = EntityState.Detached;
        return new(person.Id, person.Name, person.Birthday, Array.Empty<byte>());
    }

    /// <summary>
    /// Find Photo by Person id
    /// </summary>
    /// <param name="personId">Person primary key</param>
    /// <returns>Return byte array (photo)</returns>
    public byte[]? GetPhoto(int personId)
    {
        return _db.Persons.Where(p => p.Id == personId).Select(p => p.Photo).FirstOrDefault();
    }

    public bool Add(Person addPerson)
    {
        _db.Persons.Add(addPerson ?? throw new ArgumentNullException());
        return _db.SaveChanges() > 0;
    }

    public bool Remove(Person removePerson)
    {
        _db.Persons.Remove(removePerson);
        return _db.SaveChanges() > 0;
    }

    public bool Update(Person updatePerson)
    {
        if (!_db.Persons.Any(p => p.Id == updatePerson.Id)) return false;
        _db.Persons.Update(updatePerson);
        return _db.SaveChanges() > 0;
    }
}