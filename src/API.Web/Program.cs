using API.Core.Interfaces;
using API.Core.Services;
using API.Infrastructure.Database.DbContexts;
using API.Infrastructure.Database.Repositories;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors();

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new ArgumentNullException();
builder.Services.AddDbContext<PersonDbContext>(options => options.UseNpgsql(connectionString));

builder.Services.AddScoped<IPersonRepository, DatabasePersonRepository>();
builder.Services.AddScoped<IPersonService, PersonService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

var allowedOriginsString = app.Configuration["CORS:AllowedOrigins"] ?? throw new ArgumentNullException();
var allowedOrigins = allowedOriginsString.Trim().Split(",");
app.UseCors(b => b
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowCredentials()
    .WithOrigins(allowedOrigins));

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();