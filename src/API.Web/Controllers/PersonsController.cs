using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Core.Dtos;
using API.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace API.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private readonly IPersonService _service;
        
        public PersonsController(IPersonService service)
        {
            _service = service;
        }
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<GetPersonDto>> GetAll([FromQuery]bool nearest)
        {
            var result = nearest ? _service.GetNearest() : _service.GetAll();
            return result.ToList();
        }

        [HttpGet("{id:int}/photo")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetPhoto(int id)
        {
            var photo = _service.GetPhoto(id);
            if (photo.Photo is null) return BadRequest();
            return File(photo.Photo, "image/png");
        }

        [HttpGet("{id:int}")]
        public ActionResult<GetPersonDto> Get(int id)
        {
            var dto = _service.Get(id);
            if (dto is null) return BadRequest();
            return dto;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Add([FromForm]AddPersonDto dto)
        {
            var status = _service.Add(dto);
            return status ? CreatedAtAction(nameof(Add), dto) : BadRequest();
        }

        [HttpDelete("{id:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Remove(int id)
        {
            _service.Remove(new() {Id = id});
            return NoContent();
        }
        
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update([FromForm]UpdatePersonDto dto)
        {
            var status = _service.Update(dto);
            return status ? NoContent() : BadRequest();
        }
    }
}
